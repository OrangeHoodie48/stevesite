﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using SteveSite.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite
{
    public class OurBlogSiteDbContextFactory : IDesignTimeDbContextFactory<OurBlogSiteDbContext>
    {
        public OurBlogSiteDbContextFactory()
        {

        }

        IConfigurationRoot config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();

        public OurBlogSiteDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<OurBlogSiteDbContext>();
            optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));
            return new OurBlogSiteDbContext(optionsBuilder.Options);
        }

    }
}
