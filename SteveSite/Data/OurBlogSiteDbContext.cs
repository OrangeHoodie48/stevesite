﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SteveSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Data
{
    public class OurBlogSiteDbContext : IdentityDbContext<IdentityModel>
    {
        public OurBlogSiteDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Draft> Drafts { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Ebook> Ebooks { get; set; }
        public DbSet<CustomerEmail> CustomerEmails { get; set; }
        public DbSet<RegistrationForm> RegistrationForms { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<SubTopic> SubTopics { get; set; }
        public DbSet<Tag> Tags { get; set; }
    }
}
