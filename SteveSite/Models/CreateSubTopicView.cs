﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class CreateSubTopicView
    {
        [Required]
        public string SubTopicName { get; set; }
        [Required]
        public string TopicName { get; set; }
        public List<string> Topics { get; set; }
        public IFormFile Image { get; set; }
    }
}
