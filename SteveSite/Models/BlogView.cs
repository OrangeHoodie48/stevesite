﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class BlogView
    {
        [Required]
        public int PostId { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string TopicName { get; set; }
        [Required]
        public string Body { get; set; }
        public List<Post> Posts { get; set; }
        public List<Comment> Comments { get; set; }
        public List<Ebook> Ebooks { get; set; }
        public string Email { get; set; }
        public List<string> TagNames { get; set; }
        public string ImageUrl { get; set; }
        public Dictionary<string, string> TagMap { get; set; }
    }
}
