﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class EmailModel
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public int SubscriberNum { get; set; }
    }
}
