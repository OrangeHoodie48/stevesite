﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{

    //Working on this but basically it will need to both hold information and be used for form validation. 
    public class PostViewModel
    {
        public Post Post { get; set; }

        [Required]
        public string UserName { get; set; }
        [Required]
        public string TopicName { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public int PostId { get; set; }
        public List<Comment> Comments { get; set; }


    }
}
