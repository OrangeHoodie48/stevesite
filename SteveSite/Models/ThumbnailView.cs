﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class ThumbnailView
    {
        [Required]
        public int DraftId { get; set; }
        [Required]
        public IFormFile Thumbnail {get; set;}
    }
}
