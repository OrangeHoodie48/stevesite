﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class AllTopicsView
    {
        public List<Topic> Topics { get; set; }
        public List<SubTopic> SubTopics { get; set; }

    }
}
