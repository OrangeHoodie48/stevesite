﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class EbookView
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string BriefDescription { get; set; }
        [Required]
        public IFormFile EbookFile { get; set; }
        [Required]
        public string TopicName { get; set; }
        [Required]
        public string FullDescription { get; set; }
        [Required]
        public string ToolTip { get; set; }
        public List<string> TopicNames { get; set; }

        public async Task<Ebook> LoadEbook(Ebook ebook)
        {
            ebook.Title = Title;
            ebook.BriefDescription = BriefDescription;
            ebook.TopicName = TopicName;
            ebook.FullDescription = FullDescription;
            ebook.ToolTip = ToolTip;
            using (MemoryStream mem = new MemoryStream())
            {
                await EbookFile.CopyToAsync(mem);
                ebook.EbookBytes = mem.ToArray();
            }
            return ebook;
        }
    }
}
