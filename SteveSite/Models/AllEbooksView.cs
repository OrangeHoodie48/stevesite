﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class AllEbooksView
    {
        public List<Ebook> Ebooks { get; set; }
        public string Email { get; set; }
        public Ebook Ebook { get; set; }
    }
}
