﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class CustomerEmail
    {
        public int CustomerEmailId { get; set; }
        public string Email { get; set; }
        public bool EmailList { get; set; }
        public string VerificationCode { get; set; }
    }
}
