﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class Topic
    {
        public int TopicId { get; set; }
        [Required]
        public string TopicName { get; set; }
        [Required]
        public bool Archived { get; set; }
        public string ImageUrl { get; set; }
    }
}
