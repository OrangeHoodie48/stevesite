﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class Post : Article
    {
        [Required]
        public int PostId { get; set; }
        [Required]
        public string Date { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public override string Body { get; set; }
        [Required]
        public string TopicName { get; set; }
        public override string Links { get; set; }
        public override string Image1 { get; set; }
        public override string Image2 { get; set; }
        public override string Image3 { get; set; }
        public override string Image4 { get; set; }
        public override string Image5 { get; set; }
        public override string Tags { get; set; }
        public override string ThumbnailUrl { get; set; }
    }
}