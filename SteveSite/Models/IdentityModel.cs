﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class IdentityModel : IdentityUser
    {
        public int Level { get; set; }
    }
}
