﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class Ebook
    {
        public int EbookId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string BriefDescription { get; set; }
        [Required]
        public byte[] EbookBytes { get; set; }
        [Required]
        public string TopicName { get; set; }
        [Required]
        public string FullDescription { get; set; }
        [Required]
        public string ToolTip { get; set; }
        public string Tags { get; set; }

        public static void AddTag(Ebook ebook, string tag)
        {
            ebook.Tags += tag + "Deljdf!@#";
        }


        public static string[] GetTags(Ebook ebook)
        {
            if (ebook.Tags == null || ebook.Tags == "") return null;
            string[] tags = new string[10];
            string[] splitArray = ebook.Tags.Split("Deljdf!@#");
            for (int i = 0; i < 10; i++)
            {
                if (i < splitArray.Count() - 1)
                {
                    tags[i] = splitArray[i];
                }
                else
                {
                    tags[i] = "";
                }
            }
            return tags;
        }
    }
}
