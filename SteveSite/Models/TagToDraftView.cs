﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class TagToDraftView
    {
        public List<string> Tags { get; set; }
        [Required]
        public string TagName { get; set; }
        public int TagId { get; set; }
        public int DraftID { get; set; }
    }
}
