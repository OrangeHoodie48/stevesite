﻿using Microsoft.AspNetCore.Http;
using SteveSite.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class DraftView
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public int DraftId { get; set; }
        public string Body { get; set; }
        public string[] Links { get; set; }
        [Required]
        public string TopicName { get; set; }
        public IFormFile Image1 { get; set; }
        public IFormFile Image2 { get; set; }
        public IFormFile Image3 { get; set; }
        public IFormFile Image4 { get; set; }
        public IFormFile Image5 { get; set; }

        public List<string> TopicNames { get; set; }


        public DraftView()
        {
            Links = new string[10];
        }

        public async Task<bool> LoadDraft(Draft draft, IStoreImageService imgSer)
        {
            draft.Body = Body;
            draft.Title = Title;
            draft.TopicName = TopicName;
            if (Image1 != null)
            {
                string fileName = "";
                byte[] Img;
                using (MemoryStream mem = new MemoryStream())
                {
                    await Image1.CopyToAsync(mem);
                    Img = mem.ToArray();
                    fileName = Image1.FileName;
                }

                string url = await imgSer.StoreImage(Img, fileName);
                draft.Image1 = url;
            }
            if (Image2 != null)
            {
                string fileName = "";
                byte[] Img;
                using (MemoryStream mem = new MemoryStream())
                {
                    await Image2.CopyToAsync(mem);
                    Img = mem.ToArray();
                    fileName = Image2.FileName;
                }

                string url = await imgSer.StoreImage(Img, fileName);
                draft.Image2 = url;
            }

            if (Image3 != null)
            {
                string fileName = "";
                byte[] Img;
                using (MemoryStream mem = new MemoryStream())
                {
                    await Image3.CopyToAsync(mem);
                    Img = mem.ToArray();
                    fileName = Image3.FileName;
                }

                string url = await imgSer.StoreImage(Img, fileName);
                draft.Image3 = url;

            }
            if (Image4 != null)
            {
                string fileName = "";
                byte[] Img;
                using (MemoryStream mem = new MemoryStream())
                {
                    await Image4.CopyToAsync(mem);
                    Img = mem.ToArray();
                    fileName = Image4.FileName;
                }

                string url = await imgSer.StoreImage(Img, fileName);
                draft.Image4 = url;

            }
            if (Image5 != null)
            {
                string fileName = "";
                byte[] Img;
                using (MemoryStream mem = new MemoryStream())
                {
                    await Image5.CopyToAsync(mem);
                    Img = mem.ToArray();
                    fileName = Image5.FileName;
                }

                string url = await imgSer.StoreImage(Img, fileName);
                draft.Image5 = url;

            }

            draft.Links = "";
            foreach (string str in Links)
            {
                if (string.IsNullOrEmpty(str)) continue;
                Article.AddLink(draft, str);
            }

            return true;
        }
    }
}
