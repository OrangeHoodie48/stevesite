﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class CommentView
    {
        [Required]
        public int PostId { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string TopicName { get; set; }
        [Required]
        public string Body { get; set; }

    }
}
