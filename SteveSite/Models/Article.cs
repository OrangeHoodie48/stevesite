﻿using SteveSite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SteveSite.Models
{
    public class Article
    {

        public virtual string Body { get; set; }
        public virtual string Links { get; set; }
        public virtual string Image1 { get; set; }
        public virtual string Image2 { get; set; }
        public virtual string Image3 { get; set; }
        public virtual string Image4 { get; set; }
        public virtual string Image5 { get; set; }
        public virtual string Tags { get; set; }
        public virtual string ThumbnailUrl {get; set;}

        static string Delimeter = "akAdi_koaisudfj'390";
        public virtual string ImageUrls { get; set; }


        public string[] GetUrls()
        {
            if (String.IsNullOrEmpty(ImageUrls))
            {
                return null;
            }

            return ImageUrls.Split(Delimeter);

        }

        public void AddUrl(string url, string fileName)
        {
            ImageUrls += url + "/" + fileName + Delimeter;
        }

        public void RemoveUrl(string url, string fileName)
        {
            url += "/" + fileName;
            List<string> urls = ImageUrls.Split(Delimeter).ToList();
            urls.Remove(fileName);
            ImageUrls = String.Join(Delimeter, urls);
        }


        public static void AddTag(Article article, string tag)
        {
            article.Tags += tag + "Deljdf!@#";
        }

        public static void AddLink(Article article, string link)
        {
            article.Links += link + "Deljdf!@#";
        }

        public static string[] GetTags(Article article)
        {
            if (article.Tags == null || article.Tags == "") return null;
            string[] splitArray = article.Tags.Split("Deljdf!@#");
            string[] tags = new string[splitArray.Length -1];
            for (int i = 0; i < splitArray.Length - 1; i++)
            {
                if (i < splitArray.Count() - 1)
                {
                    tags[i] = splitArray[i];
                }
                else
                {
                    tags[i] = "";
                }
            }
            return tags;
        }

        public static string[] GetLinks(Article article)
        {
            if (article.Links == null || article.Links == "") return null;
            string[] links = new string[10];
            string[] splitArray = article.Links.Split("Deljdf!@#");
            for (int i = 0; i < 10; i++)
            {
                if (i < splitArray.Count() - 1)
                {
                    links[i] = splitArray[i];
                }
                else
                {
                    links[i] = "";
                }
            }
            return links;
        }


        public static string GetBody(Article article)
        {
            string[] images = new string[5];
            images[0] = article.Image1;
            images[1] = article.Image2;
            images[2] = article.Image3;
            images[3] = article.Image4;
            images[4] = article.Image5;
            string body = article.Body;
            body = HtmlProcessor.ImageInserter(body, images);
            return body;
        }


        public static Article RemoveTag(Article article, int tagId)
        {
            if (String.IsNullOrEmpty(article.Tags))
            {
                return null;
            }

            string[] tags = article.Tags.Split("Deljdf!@#");
            int strToInt;
            int index = 0;
            string newTags = "";
            bool hasTag = false;
            foreach (string tag in tags)
            {
                int.TryParse(tag, out strToInt);
                if (tagId == strToInt)
                {
                    hasTag = true;
                    foreach (string tag2 in tags)
                    {
                        if (!tag2.Equals(strToInt + ""))
                        {
                            newTags += tag2 + "Deljdf!@#";
                        }

                    }

                }
                index++;
            }
            if (hasTag)
            {
                article.Tags = newTags;
                return article;
            }
            return null;
        }

        public string GetPreview(int previewCount = 466)
        {
            if (String.IsNullOrEmpty(Body))
            {
                return "No Preview";
            }

            string preview = "";
            string pattern = @"<p .*?>(.*?)</p>";
            Regex reg = new Regex(pattern);
            Match match = reg.Match(Body);
            preview = match.Groups[0].ToString();
            if (String.IsNullOrEmpty(preview))
            {
                preview = "No Preview";
            }
            else
            {
                string elipse = "";
                if (preview.Length > previewCount)
                {
                    preview = preview.Substring(0, previewCount);
                    elipse = "....";
                }
                preview = "<p class='dft-paragraph' >" + preview + elipse + "</p>";

            }

            return preview;
        }
    }
}

