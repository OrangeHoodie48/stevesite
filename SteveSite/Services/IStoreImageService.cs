﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Services
{
    public interface IStoreImageService
    {
        Task<string> StoreImage(byte[] image, string fileName);
    }
}
