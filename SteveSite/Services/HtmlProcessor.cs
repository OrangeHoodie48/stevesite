﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SteveSite.Services
{
    class HtmlProcessor
    {
        //Will take the body string and an array of byte arrays holding the images and insert them inside the img tags
        //and then return the new body.
        public static string ImageInserter(string body, byte[][] images)
        {
            Regex reg1 = new Regex(@"<img.*?/>");
            Regex reg2 = new Regex("src=('(.*?)')|\"(.*?)\"");

            MatchCollection imgMatches = reg1.Matches(body);
            int differenceIndex = 0;
            int times = 0;
            foreach (Match imgMatch in imgMatches)
            {

                Match innerMatch = reg2.Match(imgMatch.Value);

                string imgNumStr = innerMatch.Groups[2].Value;
                int startIndex = imgMatch.Index + innerMatch.Groups[2].Index + differenceIndex;
                int endIndex = startIndex + innerMatch.Groups[2].Length;
                Console.WriteLine(imgMatch.ToString() + " " + imgMatch.Index + " " + innerMatch.Groups[2].ToString() + " " + innerMatch.Groups[2].Index);
                if (innerMatch.Groups[1].Length == 0)
                {
                    imgNumStr = innerMatch.Groups[3].Value;
                    startIndex = imgMatch.Index + innerMatch.Groups[3].Index + differenceIndex;
                    endIndex = startIndex + innerMatch.Groups[3].Length;
                }

                if (!int.TryParse(imgNumStr, out int imgNum))
                {
                    throw new InvalidOperationException("Inside ImageInserter Inside HtmlProcessor," +
                        " we failed to extract the image number from the body string. The value we tried to convert was: " +
                        imgNumStr);
                }

                body = body.Remove(startIndex, endIndex - startIndex);
                body = body.Insert(startIndex, "data:image;base64, " + Convert.ToBase64String(new System.ReadOnlySpan<byte>(images[imgNum])));
                differenceIndex += Convert.ToBase64String(images[imgNum]).Length + 19 - imgNumStr.Length;
                times++;
            }
            return body;
        }

        public static string ImageInserter(string body, string[] images)
        {
            Regex reg1 = new Regex(@"<img.*?/>");
            Regex reg2 = new Regex("src=('(.*?)')|\"(.*?)\"");

            MatchCollection imgMatches = reg1.Matches(body);
            int differenceIndex = 0;
            int times = 0;
            foreach (Match imgMatch in imgMatches)
            {

                Match innerMatch = reg2.Match(imgMatch.Value);

                string imgNumStr = innerMatch.Groups[2].Value;
                int startIndex = imgMatch.Index + innerMatch.Groups[2].Index + differenceIndex;
                int endIndex = startIndex + innerMatch.Groups[2].Length;
                Console.WriteLine(imgMatch.ToString() + " " + imgMatch.Index + " " + innerMatch.Groups[2].ToString() + " " + innerMatch.Groups[2].Index);
                if (innerMatch.Groups[1].Length == 0)
                {
                    imgNumStr = innerMatch.Groups[3].Value;
                    startIndex = imgMatch.Index + innerMatch.Groups[3].Index + differenceIndex;
                    endIndex = startIndex + innerMatch.Groups[3].Length;
                }

                if (!int.TryParse(imgNumStr, out int imgNum))
                {
                    throw new InvalidOperationException("Inside ImageInserter Inside HtmlProcessor," +
                        " we failed to extract the image number from the body string. The value we tried to convert was: " +
                        imgNumStr);
                }

                body = body.Remove(startIndex, endIndex - startIndex);
                body = body.Insert(startIndex, images[imgNum]);
                differenceIndex += images[imgNum].Length - imgNumStr.Length;
                times++;
            }
            return body;
        }
    }
}
