﻿using Microsoft.EntityFrameworkCore;
using SteveSite.Data;
using SteveSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Services
{
    public class DataServices : IDataServices
    {
        OurBlogSiteDbContext context;
        public DataServices(OurBlogSiteDbContext context)
        {
            this.context = context;
        }

        public void AddDraft(Draft draft)
        {
            context.Drafts.Add(draft);
            context.SaveChanges();
        }
        public List<Draft> GetDrafts()
        {
            return context.Drafts.ToList();
        }
        public Draft GetDraft(int id)
        {
            return context.Drafts.FirstOrDefault(r => r.DraftId == id);
        }

        public Draft RemoveDraft(int id)
        {
            Draft draft = context.Drafts.FirstOrDefault(r => r.DraftId == id);
            context.Drafts.Remove(draft);
            context.SaveChanges();
            return draft;
        }
        public Post PublishDraft(int id)
        {
            Draft draft = context.Drafts.FirstOrDefault(r => r.DraftId == id);
            Post post;
            if (draft.PostId.Equals("None"))
            {
                post = new Post();
                post.Body = draft.Body;
                post.Date = DateTime.Now.ToString();
                post.Title = draft.Title;
                post.Links = draft.Links;
                post.Image1 = draft.Image1;
                post.Image2 = draft.Image2;
                post.Image3 = draft.Image3;
                post.Image4 = draft.Image4;
                post.Image5 = draft.Image5;
                post.Tags = draft.Tags;
                post.ThumbnailUrl = draft.ThumbnailUrl;
                post.TopicName = draft.TopicName;
                context.Posts.Add(post);
                context.Drafts.Remove(draft);
                context.SaveChanges();
                return post;
            }

            post = context.Posts.FirstOrDefault(r => r.PostId.Equals(int.Parse(draft.PostId)));
            post.Body = draft.Body;
            post.Title = draft.Title;
            post.Links = draft.Links;
            post.Image1 = draft.Image1;
            post.Image2 = draft.Image2;
            post.Image3 = draft.Image3;
            post.Image4 = draft.Image4;
            post.Image5 = draft.Image5;
            post.Tags = draft.Tags;
            post.TopicName = draft.TopicName;
            context.Posts.Attach(post).State = EntityState.Modified;
            context.Drafts.Remove(draft);
            context.SaveChanges();
            return post;
        }

        public List<Post> GetPosts()
        {
            return context.Posts.ToList();
        }

        public Post RemovePost(int id)
        {
            Post post = context.Posts.FirstOrDefault(r => r.PostId == id);
            context.Posts.Remove(post);
            context.SaveChanges();
            return post;
        }

        public Draft UpdateDraft(Draft draft)
        {
            context.Drafts.Attach(draft).State = EntityState.Modified;
            context.SaveChanges();
            return draft;
        }

        public Post GetPost(int id)
        {
            return context.Posts.FirstOrDefault(r => r.PostId == id);
        }

        public Post UpdatePost(Post post)
        {
            context.Attach(post).State = EntityState.Modified;
            context.SaveChanges();
            return post;
        }

        public Ebook GetEbook(int id)
        {
            return context.Ebooks.FirstOrDefault(r => r.EbookId == id);
        }
        public List<Ebook> GetEbooks()
        {
            return context.Ebooks.ToList();
        }
        public Ebook RemoveEbook(int id)
        {
            Ebook ebook = context.Ebooks.FirstOrDefault(r => r.EbookId == id);
            context.Ebooks.Remove(ebook);
            context.SaveChanges();
            return ebook;
        }

        public Ebook AddEbook(Ebook ebook)
        {
            context.Ebooks.Add(ebook);
            context.SaveChanges();
            return ebook;
        }
        public CustomerEmail AddCustomerEmail(CustomerEmail customerEmail)
        {
            if (context.CustomerEmails.FirstOrDefault(r => r.Email == customerEmail.Email) != null)
            {
                return null;
            }

            Random rand = new Random();
            string verificationCode = "";
            for (int i = 0; i < 26; i++)
            {
                int lot = rand.Next(3);
                if (lot == 0)
                {
                    char c = (char)((rand.Next(9) + 1) + 47);
                    verificationCode += c;
                }
                else if (lot == 1)
                {
                    char c = (char)((rand.Next(25) + 1) + 64);
                    verificationCode += c;
                }
                else
                {
                    char c = (char)((rand.Next(25) + 1) + 96);
                    verificationCode += c;
                }
            }

            customerEmail.VerificationCode = verificationCode;
            context.CustomerEmails.Add(customerEmail);
            context.SaveChanges();
            return customerEmail;
        }

        public RegistrationForm GetRegistrationForm(int id)
        {
            return context.RegistrationForms.FirstOrDefault(r => r.RegistrationFormId == id);
        }

        public RegistrationForm AddRegistrationForm(RegistrationForm form)
        {
            if (context.RegistrationForms.FirstOrDefault(r => r.Email.Equals(form.Email)) == null)
            {
                if (context.RegistrationForms.FirstOrDefault(r => r.Email.Equals(form.Password)) == null)
                {
                    context.RegistrationForms.Add(form);
                    context.SaveChanges();
                    return form;
                }
            }
            return null;
        }

        public RegistrationForm RemoveRegistrationForm(RegistrationForm form)
        {
            context.RegistrationForms.Remove(form);
            context.SaveChanges();
            return form;
        }

        public RegistrationForm GetRegistrationForm(string email, string password, string registrationCode)
        {
            RegistrationForm form = context.RegistrationForms.FirstOrDefault(r => r.Email.Equals(email));
            if (form != null)
            {
                if (form.Password.Equals(password) && form.RegistrationCode.Equals(registrationCode))
                {
                    return form;
                }
            }
            return null;
        }

        public Comment GetComment(int id)
        {
            return context.Comments.FirstOrDefault(r => r.CommentId == id);
        }

        public Comment AddComment(Comment comment)
        {
            context.Comments.Add(comment);
            context.SaveChanges();
            return comment;
        }

        public void RemoveComment(int id)
        {
            context.Remove(context.Comments.FirstOrDefault(r => r.CommentId == id));
            context.SaveChanges();
        }

        public List<Comment> GetComments()
        {
            return context.Comments.ToList();
        }

        public Topic AddTopic(Topic topic)
        {
            if (context.Topics.FirstOrDefault(r => r.TopicName.Equals(topic.TopicName)) != null) return null;
            if (context.SubTopics.FirstOrDefault(r => r.SubTopicName.Equals(topic.TopicName)) != null) return null;
            topic.Archived = false;
            context.Topics.Add(topic);
            context.SaveChanges();
            return topic;
        }

        public Topic GetTopic(int id)
        {
            return context.Topics.FirstOrDefault(r => r.TopicId == id);
        }

        public List<Topic> GetTopics()
        {
            return context.Topics.Where(r => r.Archived == false).ToList();
        }

        public Topic RemoveTopic(int id)
        {
            Topic topic = context.Topics.FirstOrDefault(r => r.TopicId == id);
            if (topic == null) return null;
            context.Topics.Remove(topic);
            List<SubTopic> subTopics = context.SubTopics.Where(r => r.TopicName.Equals(topic.TopicName)).ToList();
            if (subTopics != null)
            {
                foreach (SubTopic sT in subTopics)
                {
                    RemoveSubTopic(sT.SubTopicId);
                }
            }
            context.SaveChanges();
            return null;
        }

        public SubTopic AddSubTopic(SubTopic subTopic)
        {
            if (context.SubTopics.FirstOrDefault(r => r.SubTopicName.Equals(subTopic.SubTopicName)) != null)
            {
                return null;
            }
            if (context.Topics.FirstOrDefault(r => r.TopicName.Equals(subTopic.SubTopicName)) != null)
            {
                return null;
            }
            subTopic.Archived = false;
            context.SubTopics.Add(subTopic);
            context.SaveChanges();
            return subTopic;
        }

        public SubTopic GetSubTopic(int id)
        {
            return context.SubTopics.FirstOrDefault(r => r.SubTopicId == id);
        }

        public List<SubTopic> GetSubTopics()
        {
            return context.SubTopics.Where(r => r.Archived == false).ToList();
        }

        public SubTopic RemoveSubTopic(int id)
        {
            SubTopic subTopic = context.SubTopics.FirstOrDefault(r => r.SubTopicId == id);
            if (subTopic == null) return null;
            context.SubTopics.Remove(subTopic);
            context.SaveChanges();
            return null;
        }
        public List<SubTopic> GetSubTopicsFor(string topicName)
        {
            return context.SubTopics.Where(r => r.TopicName.Equals(topicName)).Where(r => r.Archived == false).ToList();
        }

        public List<string> GetAllTopicAndSubTopicNames()
        {
            List<string> names = new List<string>();
            foreach (Topic topic in context.Topics.Where(r => r.Archived == false))
            {
                names.Add(topic.TopicName);
                foreach (SubTopic subTopic in GetSubTopicsFor(topic.TopicName))
                {
                    names.Add(subTopic.SubTopicName);
                }
            }
            return names;
        }

        public List<Post> GetAllPostsForTopicOrSubTopic(string topicName)
        {
            Topic topic = context.Topics.FirstOrDefault(r => r.TopicName.Equals(topicName));
            if (topic == null)
            {
                SubTopic subTopic = context.SubTopics.FirstOrDefault(r => r.SubTopicName.Equals(topicName));
                return context.Posts.Where(r => r.TopicName.Equals(topicName)).ToList();
            }

            List<Post> posts = context.Posts.Where(r => r.TopicName.Equals(topicName)).ToList();
            List<SubTopic> subTopics = context.SubTopics.Where(r => r.TopicName.Equals(topicName) && r.Archived == false).ToList();
            foreach (SubTopic subTopic in subTopics)
            {
                posts = posts.Concat(context.Posts.Where(r => r.TopicName.Equals(subTopic.SubTopicName))).ToList();
            }

            return posts.OrderByDescending(r => Convert.ToDateTime(r.Date)).ToList();
        }

       

        public Topic ToggleArchivedForTopic(int id)
        {
            Topic topic = context.Topics.FirstOrDefault(r => r.TopicId == id);
            List<SubTopic> subTopics = context.SubTopics.Where(r => r.TopicName.Equals(topic.TopicName)).ToList();
            if (topic.Archived)
            {
                topic.Archived = false;
                foreach (SubTopic subTopic in subTopics)
                {
                    subTopic.Archived = false;
                    context.SubTopics.Attach(subTopic).State = EntityState.Modified;
                }
            }
            else
            {
                topic.Archived = true;
                foreach (SubTopic subTopic in subTopics)
                {
                    subTopic.Archived = true;
                    context.SubTopics.Attach(subTopic).State = EntityState.Modified;

                }
            }
            context.Topics.Attach(topic).State = EntityState.Modified;
            context.SaveChanges();
            return topic;
        }

        public SubTopic ToggleArchivedForSubTopic(int id)
        {
            SubTopic subTopic = context.SubTopics.FirstOrDefault(r => r.SubTopicId == id);
            if (subTopic.Archived)
            {
                subTopic.Archived = false;
            }
            else
            {
                subTopic.Archived = true;
            }
            context.SubTopics.Attach(subTopic).State = EntityState.Modified;
            context.SaveChanges();
            return subTopic;
        }

        public List<Topic> GetArchivedTopics()
        {
            return context.Topics.Where(r => r.Archived == true).ToList();
        }

        public List<SubTopic> GetArchivedSubTopics()
        {
            return context.SubTopics.Where(r => r.Archived == true).ToList();
        }

        public Tag CreateTag(string tag)
        {
            if (context.Tags.FirstOrDefault(r => r.TagName.ToUpper().Equals(tag.ToUpper())) != null)
            {
                throw new Exception("Tag name taken");
            }
            Tag obj = new Tag();
            obj.TagName = tag;
            context.Tags.Add(obj);
            context.SaveChanges();
            return obj;
        }

        public List<Post> GetAllUnArchivedPosts()
        {
            List<string> names = context.Topics.Where(t => t.Archived == true).Select(t => t.TopicName).ToList();
            names.AddRange(context.SubTopics.Where(s => s.Archived == true).Select(s => s.SubTopicName).ToList());
            return context.Posts.Where(p => !names.Contains(p.TopicName)).ToList();
        }

        public Tag RemoveTag(int id)
        {
            foreach (Post post in context.Posts)
            {
                if (Article.RemoveTag(post, id) != null)
                {
                    context.Posts.Attach(post).State = EntityState.Modified;
                }
            }

            Tag tag = context.Tags.FirstOrDefault(r => r.TagId == id);
            context.Tags.Remove(tag);
            context.SaveChanges();

            return tag;
        }

        public List<Tag> GetTags()
        {
            return context.Tags.ToList();
        }

        public List<string> GetTagNames()
        {
            List<string> names = new List<string>();
            foreach (Tag tag in context.Tags)
            {
                names.Add(tag.TagName);
            }

            return names;
        }

        public int GetTagId(string tagName)
        {
            Tag tag = context.Tags.FirstOrDefault(r => r.TagName.Equals(tagName));
            return tag.TagId;
        }

        public Tag GetTag(int id)
        {
            return context.Tags.FirstOrDefault(r => r.TagId == id);
        }

        public string GetTopicSubTopicImage(string name)
        {
            string imageUrl;
            Topic topic = context.Topics.FirstOrDefault(r => r.TopicName.Equals(name));
            if (topic != null)
            {
                if (topic.ImageUrl != null)
                {
                    imageUrl = topic.ImageUrl;
                    return imageUrl;
                }
                return null;
            }

            SubTopic subTopic = context.SubTopics.FirstOrDefault(r => r.SubTopicName.Equals(name));
            if (subTopic != null)
            {
                if (subTopic.ImageUrl != null)
                {
                    imageUrl = subTopic.ImageUrl;
                    return imageUrl;
                }
            }
            return null;
        }

        public List<string> GetEmailList()
        {
            List<string> emails = new List<string>();
            foreach (CustomerEmail email in context.CustomerEmails)
            {
                if (email.EmailList)
                {
                    emails.Add(email.Email);
                }
            }

            if (emails.Count == 0) return null;

            return emails;
        }

        public List<CustomerEmail> GetCustomerEmailsOnList()
        {
            return context.CustomerEmails.Where(r => r.EmailList == true).ToList();
        }

        public CustomerEmail GetCustomerEmailByVerificationCode(string verificationCode)
        {
            return context.CustomerEmails.FirstOrDefault(r => r.VerificationCode.Equals(verificationCode));
        }

        public CustomerEmail UpdateCustomerEmail(CustomerEmail customer)
        {
            context.CustomerEmails.Attach(customer).State = EntityState.Modified;
            context.SaveChanges();
            return customer;
        }

        public List<Comment> GetCommentsForPost(int id)
        {
            IQueryable<Comment> comments = context.Comments.Where(r => r.PostId == id);
            return comments.ToList();
        }

        public Dictionary<string, string> GetTagMap()
        {
            List<string> tagNames = GetTagNames();
            
            Dictionary<string, string> tagMap = new Dictionary<string, string>(); 
            foreach(string tagName in tagNames)
            {
                tagMap.Add(GetTagId(tagName) + "", tagName);
            }
            return tagMap; 
        }
        
    }
}
