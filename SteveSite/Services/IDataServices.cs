﻿using SteveSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Services
{
    public interface IDataServices
    {
        void AddDraft(Draft draft);
        List<Draft> GetDrafts();
        Draft GetDraft(int id);
        Draft RemoveDraft(int id);
        Post PublishDraft(int id);
        List<Post> GetPosts();
        Post RemovePost(int id);
        Draft UpdateDraft(Draft draft);
        Post GetPost(int id);
        Post UpdatePost(Post post);
        Ebook GetEbook(int id);
        List<Ebook> GetEbooks();
        Ebook RemoveEbook(int id);
        Ebook AddEbook(Ebook ebook);
        CustomerEmail AddCustomerEmail(CustomerEmail customerEmail);
        RegistrationForm AddRegistrationForm(RegistrationForm form);
        RegistrationForm RemoveRegistrationForm(RegistrationForm form);
        RegistrationForm GetRegistrationForm(int id);
        RegistrationForm GetRegistrationForm(string email, string password, string registrationCode);
        Comment AddComment(Comment comment);
        Comment GetComment(int id);
        void RemoveComment(int id);
        List<Comment> GetComments();
        Topic AddTopic(Topic topic);
        Topic GetTopic(int id);
        List<Topic> GetTopics();
        Topic RemoveTopic(int id);
        SubTopic AddSubTopic(SubTopic SubTopic);
        SubTopic GetSubTopic(int id);
        List<SubTopic> GetSubTopics();
        SubTopic RemoveSubTopic(int id);
        List<SubTopic> GetSubTopicsFor(string topicName);
        List<string> GetAllTopicAndSubTopicNames();
        List<Post> GetAllPostsForTopicOrSubTopic(string topicName);
        SubTopic ToggleArchivedForSubTopic(int id);
        Topic ToggleArchivedForTopic(int id);
        List<Post> GetAllUnArchivedPosts(); 
        List<Topic> GetArchivedTopics();
        List<SubTopic> GetArchivedSubTopics();
        Tag CreateTag(string tag);
        Tag RemoveTag(int id);
        List<Tag> GetTags();
        List<string> GetTagNames();
        int GetTagId(string tagName);
        Tag GetTag(int id);
        string GetTopicSubTopicImage(string name);
        List<string> GetEmailList();
        List<CustomerEmail> GetCustomerEmailsOnList();
        CustomerEmail GetCustomerEmailByVerificationCode(string verificationCode);
        CustomerEmail UpdateCustomerEmail(CustomerEmail customer);
        List<Comment> GetCommentsForPost(int id);
        Dictionary<string, string> GetTagMap(); 
    }
}
