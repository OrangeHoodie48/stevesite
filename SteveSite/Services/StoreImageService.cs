﻿using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Services
{
    public class StoreImageService : IStoreImageService
    {
        IConfiguration cnfg;
        public StoreImageService(IConfiguration cnfg)
        {
            this.cnfg = cnfg;
        }

        public async Task<string> StoreImage(byte[] image, string fileName)
        {
            fileName = Path.GetFileName(fileName);
            string url = cnfg["BlobServiceUrl"] + "/" + fileName;

            var creds = new StorageCredentials(cnfg["BlobServiceName"], cnfg["BlobServiceKey"]);
            var blob = new CloudBlockBlob(new Uri(url), creds);

            if (await blob.ExistsAsync())
            {
                await blob.FetchAttributesAsync();
                if (blob.Properties.Length == image.Length)
                {
                    return url;
                }
            }

            await blob.UploadFromByteArrayAsync(image, 0, image.Length);
            return url;
        }
    }
}
