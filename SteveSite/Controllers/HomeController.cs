﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SteveSite.Models;
using SteveSite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SteveSite.Controllers
{
    public class HomeController : Controller
    {
        IDataServices dataService;
        UserManager<IdentityModel> userManager;
        public HomeController(IDataServices dataServices, UserManager<IdentityModel> userManager)
        {
            this.dataService = dataServices;
            this.userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            if (userManager.Users.Count() == 0)
            {
                IdentityModel user = new IdentityModel();
                user.Email = "daddarios@dupage.edu";
                user.Level = 1;
                user.UserName = "Admin";
                await userManager.CreateAsync(user, "f1ght1ngTh8nder!");
            }

            BlogView view = new BlogView();
            view.TopicName = "Index";
            view.TagNames = dataService.GetTagNames();
            view.TagMap = dataService.GetTagMap();
            
          
            List<Post> posts = dataService.GetAllUnArchivedPosts().OrderByDescending(r => Convert.ToDateTime(r.Date)).ToList();
            if (posts == null)
            {
                view.Posts = null;
                return View(view);
            }
            view.Posts = posts;
            view.Comments = dataService.GetComments();

            List<Ebook> ebooks = dataService.GetEbooks();
            ebooks.Reverse();
            if (ebooks.Count > 10)
            {
                ebooks = ebooks.GetRange(0, 10);
            }
            view.Ebooks = ebooks;

            return View(view);
        }

        public IActionResult TopicBlog(string topicName)
        {
            BlogView view = new BlogView();
            List<Post> posts = dataService.GetAllPostsForTopicOrSubTopic(topicName);
            view.TagNames = dataService.GetTagNames();
            view.ImageUrl = dataService.GetTopicSubTopicImage(topicName);
            view.TagMap = dataService.GetTagMap(); 
            if (dataService.GetTopics().FirstOrDefault(r => r.TopicName.Equals(topicName)) != null)
            {
                List<SubTopic> subTopics = dataService.GetSubTopics().Where(r => r.TopicName.Equals(topicName)).ToList();
                List<Ebook> allEbooks = dataService.GetEbooks();
                allEbooks.Reverse();
                List<Ebook> ebooks = new List<Ebook>();
                foreach (Ebook ebook in allEbooks)
                {
                    if (ebook.TopicName.Equals(topicName))
                    {
                        ebooks.Add(ebook);

                    }
                    else
                    {
                        SubTopic subTopic = subTopics.FirstOrDefault(r => r.SubTopicName.Equals(ebook.TopicName));
                        if (subTopic != null)
                        {
                            ebooks.Add(ebook);
                        }
                    }

                }
                view.Ebooks = ebooks;
            }
            else
            {
                view.Ebooks = dataService.GetEbooks().Where(r => r.TopicName.Equals(topicName)).ToList();
                view.Ebooks.Reverse();
            }


            if (posts == null)
            {
                view.Posts = null;
                return View(view);
            }
            view.Posts = posts;
            view.TopicName = topicName;
            view.Comments = dataService.GetComments();
            return View(view);
        }

        [HttpGet]
        public IActionResult EmailAll()
        {
            EmailModel model = new EmailModel();
            model.SubscriberNum = dataService.GetCustomerEmailsOnList().Count;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EmailAll(EmailModel model)
        {
            List<CustomerEmail> customerEmails = dataService.GetCustomerEmailsOnList();
            if (customerEmails == null) return RedirectToAction("Index", "Home");

            foreach (CustomerEmail customerEmail in customerEmails)
            {
                string body = model.Message;
                body += "\n\n\n\n" + "Visit https://localhost:44329/Home/UnsubscribeEmail?verificationCode=" + customerEmail.VerificationCode + " to unsubscribe from this email list.";

                var smtpClient = new SmtpClient
                {
                    Host = "smtp.aol.com",
                    Port = 587,
                    EnableSsl = true,
                    Credentials = new NetworkCredential("stevend39addario@aol.com", "C1c3r0Th3Th1rd")
                };
                using (var message = new MailMessage("stevend39addario@aol.com", customerEmail.Email) { Subject = model.Title, Body = body })
                {
                    smtpClient.Send(message);
                }

            }
            return RedirectToAction("Index", "Home");

        }

        public IActionResult UnsubscribeEmail(string verificationCode)
        {
            CustomerEmail customerEmail = dataService.GetCustomerEmailByVerificationCode(verificationCode);
            customerEmail.EmailList = false;
            dataService.UpdateCustomerEmail(customerEmail);
            return View("Message", "Your email has been taken off our email list.");
        }

        public IActionResult Message(string message)
        {
            return View(message);
        }
    }
}
