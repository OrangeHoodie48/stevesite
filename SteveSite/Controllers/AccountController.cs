﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SteveSite.Models;
using SteveSite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SteveSite.Controllers
{
    public class AccountController : Controller
    {
        IDataServices dataServices;
        UserManager<IdentityModel> userManager;
        SignInManager<IdentityModel> signInManager;

        public AccountController(IDataServices dataServices, UserManager<IdentityModel> userManager, SignInManager<IdentityModel> signInManager)
        {
            this.userManager = userManager;
            this.dataServices = dataServices;
            this.signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            LoginView view = new LoginView();
            return View(view);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(string email, string password)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            IdentityModel user = userManager.Users.FirstOrDefault(r => r.Email.Equals(email));
            if (user == null) { return View(); }

            var results = await signInManager.PasswordSignInAsync(user, password, true, false);
            if (!results.Succeeded)
            {
                return View();
            }



            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            RegistrationForm model = new RegistrationForm();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegistrationForm model)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            IdentityModel user = userManager.Users.FirstOrDefault(r => r.Email.Equals(model.Email));
            if (user != null)
            {
                ModelState.AddModelError("", "Sorry, that email is already associated with an account");
                return View(model);
            }

            user = new IdentityModel();
            user.Email = model.Email;
            user.UserName = model.UserName;
            user.Level = 3;

            var results = await userManager.CreateAsync(user, model.Password);
            if (!results.Succeeded)
            {
                ModelState.AddModelError("", "Sorry, either your email or password was invalid or taken. Please try again.");
                return View(model);
            }

            await userManager.DeleteAsync(user);


            Random rand = new Random();
            model.RegistrationCode = "" + Math.Round((rand.NextDouble() * 10000001));
            if (dataServices.AddRegistrationForm(model) == null)
            {
                ModelState.AddModelError("", "Sorry, either your email or password was taken. Please try again.");
                return View(model);
            }


            string body = "Hello and thank you for creating an account with us, your verification code is " + model.RegistrationCode +
                " Please visit the following link: " + "http://earl-milton-web-app.azurewebsites.net/Account/Verify";
            var smtpClient = new SmtpClient
            {
                Host = "smtp.aol.com",
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential("stevend39addario@aol.com", "C1c3r0Th3Th1rd")
            };
            using (var message = new MailMessage("stevend39addario@aol.com", model.Email) { Subject = "Account Registration", Body = body })
            {
                smtpClient.Send(message);
            }

            return RedirectToAction("RegistrationEmailSent", "Account");
        }

        [HttpGet]
        public IActionResult Verify()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            RegistrationForm model = new RegistrationForm();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Verify(RegistrationForm model)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            RegistrationForm form = dataServices.GetRegistrationForm(model.Email, model.Password, model.RegistrationCode);
            if (form == null)
            {
                ModelState.AddModelError("", "Sorry, incorrect registration information. Please Try Again.");
                return View(model);
            }

            IdentityModel user = new IdentityModel();
            user.Email = form.Email;
            user.UserName = form.UserName;
            var results = await userManager.CreateAsync(user, form.Password);
            if (!results.Succeeded)
            {
                ModelState.AddModelError("", "Sorry, incorrect registration information. Please Try Again.");
                return View(model);
            }

            CustomerEmail customerEmail = new CustomerEmail();
            customerEmail.Email = form.Email;
            customerEmail.EmailList = true;
            dataServices.AddCustomerEmail(customerEmail);
            dataServices.RemoveRegistrationForm(form);
            return RedirectToAction("AccountCreated", "Account");
        }

        public IActionResult RegistrationEmailSent()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public IActionResult AccountCreated()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
    }
}
