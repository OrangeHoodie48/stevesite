﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SteveSite.Models;
using SteveSite.Services;

namespace SteveSite.Controllers
{
    [Authorize]
    public class PostsController : Controller
    {
        IDataServices dataServices;
        public PostsController(IDataServices dataServices)
        {
            this.dataServices = dataServices;
        }

        public IActionResult AllPosts()
        {
            return View(dataServices.GetAllUnArchivedPosts().OrderByDescending(r => Convert.ToDateTime(r.Date)));
        }

        public IActionResult CloneDraft(int id)
        {
            Post post = dataServices.GetPost(id);
            Draft draft = new Draft();
            draft.PostId = post.PostId + "";
            draft.Tags = post.Tags;
            draft.Title = post.Title;
            draft.Body = post.Body;
            draft.Date = post.Date;
            draft.Links = post.Links;
            draft.Image1 = post.Image1;
            draft.Image2 = post.Image2;
            draft.Image3 = post.Image3;
            draft.Image4 = post.Image4;
            draft.Image5 = post.Image5;
            draft.TopicName = post.TopicName;
            draft.ThumbnailUrl = post.ThumbnailUrl; 
            dataServices.AddDraft(draft);
            return RedirectToAction("AllDrafts", "Drafts", dataServices.GetDrafts());
        }

        public IActionResult RemovePost(int id)
        {
            dataServices.RemovePost(id);
            return RedirectToAction("AllPosts", "Posts", dataServices.GetPosts());
        }

        [AllowAnonymous]
        public IActionResult ViewPost(int id)
        {
            Post post = dataServices.GetPost(id);
            PostViewModel dto = new PostViewModel();
            dto.Post = post;
            dto.TopicName = post.TopicName;
            List<Comment> comments = dataServices.GetCommentsForPost(post.PostId);
            dto.Comments = comments;

            return View(dto);
        }
    }
}
