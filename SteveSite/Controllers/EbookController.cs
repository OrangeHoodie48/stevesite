﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SteveSite.Models;
using SteveSite.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SteveSite.Controllers
{
    [Authorize]
    public class EbookController : Controller
    {
        IDataServices dataServices;
        public EbookController(IDataServices dataServices)
        {
            this.dataServices = dataServices;
        }


        [HttpGet]
        public IActionResult CreateEbook()
        {
            EbookView view = new EbookView();
            view.TopicNames = dataServices.GetAllTopicAndSubTopicNames();
            return View(view);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateEbook(EbookView view)
        {
            if (!ModelState.IsValid)
            {
                return View(view);
            }

            Ebook ebook = new Ebook();
            await view.LoadEbook(ebook);
            dataServices.AddEbook(ebook);
            return RedirectToAction("AllEbooks", "Ebook", dataServices.GetEbooks());
        }

        [AllowAnonymous]
        public IActionResult AllEbooks()
        {
            AllEbooksView view = new AllEbooksView();
            view.Ebooks = dataServices.GetEbooks();
            return View(view);
        }

        public IActionResult SendEbook(string email, int ebookId)
        {
            CustomerEmail customerEmail = new CustomerEmail();
            customerEmail.Email = email;
            customerEmail.EmailList = true;
            dataServices.AddCustomerEmail(customerEmail);
            string body = "This is a test ";
            Ebook ebook = dataServices.GetEbook(ebookId);

            var smtpClient = new SmtpClient
            {
                Host = "smtp.aol.com",
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential("stevend39addario@aol.com", "C1c3r0Th3Th1rd")
            };
            using (var message = new MailMessage("stevend39addario@aol.com", email) { Subject = "Your Ebook", Body = body })
            using (MemoryStream mem = new MemoryStream(ebook.EbookBytes))
            {
                Attachment attachment = new Attachment(mem, ebook.Title + ".pdf");
                message.Attachments.Add(attachment);
                smtpClient.Send(message);
            }

            return RedirectToAction("EbookSent", "Ebook");
        }

        public IActionResult EbookPage(string title)
        {
            AllEbooksView view = new AllEbooksView();
            view.Ebook = dataServices.GetEbooks().FirstOrDefault(r => r.Title.Equals(title));
            return View(view);
        }


        public IActionResult EbookSent()
        {
            return View();
        }
    }
}
