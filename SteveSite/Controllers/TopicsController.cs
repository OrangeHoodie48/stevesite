﻿using Microsoft.AspNetCore.Mvc;
using SteveSite.Models;
using SteveSite.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Controllers
{
    public class TopicsController : Controller
    {
        IDataServices dataService;
        IStoreImageService storeImageService; 
        public TopicsController(IDataServices dataService, IStoreImageService storeImageService)
        {
            this.dataService = dataService;
            this.storeImageService = storeImageService; 
        }

        [HttpGet]
        public IActionResult CreateTopic()
        {
            CreateTopicView viewModel = new CreateTopicView();
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateTopic(CreateTopicView viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            Topic topic = new Topic();
            topic.TopicName = viewModel.TopicName;
            if (viewModel.Image != null)
            {
                using (MemoryStream mem = new MemoryStream())
                {
                    await viewModel.Image.CopyToAsync(mem);
                    topic.ImageUrl = await storeImageService.StoreImage(mem.ToArray(), viewModel.Image.FileName);
                }
            }
            Topic topic2 = dataService.AddTopic(topic);
            if (topic2 == null)
            {
                ModelState.AddModelError("", "Sorry, topic name is already taken.");
                return View(viewModel);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult CreateSubtopic()
        {
            CreateSubTopicView view = new CreateSubTopicView();
            List<string> topics = new List<string>();
            foreach (Topic topic in dataService.GetTopics())
            {
                topics.Add(topic.TopicName);
            }
            view.Topics = topics;
            return View(view);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateSubTopic(CreateSubTopicView viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            SubTopic subTopic = new SubTopic();
            subTopic.SubTopicName = viewModel.SubTopicName;
            subTopic.TopicName = viewModel.TopicName;
            if (viewModel.Image != null)
            {
                using (MemoryStream mem = new MemoryStream())
                {
                    await viewModel.Image.CopyToAsync(mem);
                    subTopic.ImageUrl = await storeImageService.StoreImage(mem.ToArray(), viewModel.Image.FileName);
                }
            }
            subTopic = dataService.AddSubTopic(subTopic);
            if (subTopic == null)
            {
                ModelState.AddModelError("", "Sub-Topic name already exists.");
                return View(viewModel);
            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult AllTopics()
        {
            AllTopicsView view = new AllTopicsView();
            view.Topics = dataService.GetTopics();
            view.SubTopics = dataService.GetSubTopics();
            return View(view);
        }

        public IActionResult ToggleArchiveTopic(int id)
        {
            dataService.ToggleArchivedForTopic(id);
            return RedirectToAction("AllTopics", "Topics");
        }

        public IActionResult ToggleArchiveSubTopic(int id)
        {
            dataService.ToggleArchivedForSubTopic(id);
            return RedirectToAction("AllTopics", "Topics");
        }

        public IActionResult ArchivedTopics()
        {
            AllTopicsView view = new AllTopicsView();
            view.Topics = dataService.GetArchivedTopics();
            view.SubTopics = dataService.GetArchivedSubTopics();
            return View(view);
        }

        [HttpGet]
        public IActionResult DeleteTopic(int id)
        {
            DeleteTopicView view = new DeleteTopicView();
            view.Id = id;
            return View(view);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteTopic(DeleteTopicView view)
        {
            if (view.Text.Equals("DELETE"))
            {
                dataService.RemoveTopic(view.Id);
                return RedirectToAction("ArchivedTopics", "Topics");
            }
            ModelState.AddModelError("", "Incorect text entered");
            return View(view);
        }

        [HttpGet]
        public IActionResult DeleteSubTopic(int id)
        {
            DeleteTopicView view = new DeleteTopicView();
            view.Id = id;
            return View(view);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteSubTopic(DeleteTopicView view)
        {
            if (view.Text.Equals("DELETE"))
            {
                dataService.RemoveSubTopic(view.Id);
                return RedirectToAction("ArchivedTopics", "Topics");
            }
            ModelState.AddModelError("", "Incorect text entered");
            return View(view);
        }
    }
}
