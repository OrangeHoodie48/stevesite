﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SteveSite.Models;
using SteveSite.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Controllers
{
    [Authorize]
    public class DraftsController : Controller
    {
        IDataServices dataServices;
        IStoreImageService imgSer;
        public DraftsController(IDataServices dataServices, IStoreImageService imgSer)
        {
            this.imgSer = imgSer;
            this.dataServices = dataServices;
        }

        [HttpGet]
        public IActionResult CreateDraft()
        {
            DraftView draftView = new DraftView();
            draftView.TopicNames = dataServices.GetAllTopicAndSubTopicNames();
            return View(draftView);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateDraft(DraftView draftView)
        {
            if (!ModelState.IsValid)
            {
                return View(draftView);
            }

            Draft draft = new Draft();
            await draftView.LoadDraft(draft, imgSer);
            draft.Date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time")).ToString();
            draft.PostId = "None";
            dataServices.AddDraft(draft);
            return RedirectToAction("AllDrafts", "Drafts");
        }


        public IActionResult AllDrafts()
        {
            return View(dataServices.GetDrafts());
        }

        public IActionResult DraftPreview(int id)
        {
            Draft draft = dataServices.GetDraft(id);
            return View(draft);
        }

        public IActionResult RemoveDraft(int id)
        {
            dataServices.RemoveDraft(id);
            return RedirectToAction("AllDrafts", "Drafts", dataServices.GetDrafts());
        }

        public IActionResult PublishDraft(int id)
        {
            dataServices.PublishDraft(id);
            return RedirectToAction("AllDrafts", "Drafts", dataServices.GetDrafts());
        }

        [HttpGet]
        public IActionResult EditDraft(int id)
        {
            DraftView view = new DraftView();
            Draft draft = dataServices.GetDraft(id);
            view.Body = draft.Body;
            view.Title = draft.Title;
            view.TopicName = draft.TopicName;
            view.Links = Article.GetLinks(draft);
            view.DraftId = draft.DraftId;
            view.TopicNames = dataServices.GetAllTopicAndSubTopicNames();
            return View(view);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditDraft(DraftView view)
        {
            if (!ModelState.IsValid)
            {
                return View(view);
            }

            Draft draft = dataServices.GetDraft(view.DraftId);
            await view.LoadDraft(draft, imgSer);

            dataServices.UpdateDraft(draft);
            return RedirectToAction("AllDrafts", "Drafts", dataServices.GetDrafts());
        }

        [HttpGet] 
        public IActionResult AddThumbnailToDraft(int id)
        {
            ThumbnailView model = new ThumbnailView();
            model.DraftId = id; 
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddThumbnailToDraft(ThumbnailView model)
        {
            if (!ModelState.IsValid)
            {
                return View(model); 
            }

            Draft draft = dataServices.GetDraft(model.DraftId); 

            using(MemoryStream mem = new MemoryStream())
            {
                await model.Thumbnail.CopyToAsync(mem);
                draft.ThumbnailUrl = await imgSer.StoreImage(mem.ToArray(), model.Thumbnail.FileName);
            }
            dataServices.UpdateDraft(draft);

            return RedirectToAction("AllDrafts", "Drafts");
        }

        [HttpGet]
        public IActionResult AddTagToDraft(int id)
        {
            TagToDraftView model = new TagToDraftView();
            model.Tags = dataServices.GetTagNames();
            model.DraftID = id;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddTagToDraft(TagToDraftView model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            int tagId = dataServices.GetTagId(model.TagName);
            Draft draft = dataServices.GetDraft(model.DraftID);
            Article.AddTag(draft, tagId + "");
            dataServices.UpdateDraft(draft);
            return RedirectToAction("AllDrafts", "Drafts");
        }

        [HttpGet]
        public IActionResult RemoveTagFromDraft(int id)
        {
            TagToDraftView model = new TagToDraftView();
            Draft draft = dataServices.GetDraft(id);
            List<string> tagNames = new List<string>();
            foreach (string tagId in Article.GetTags(draft))
            {
                if (String.IsNullOrEmpty(tagId)) continue;

                string tagName = dataServices.GetTag(int.Parse(tagId)).TagName;
                tagNames.Add(tagName);
            }
            model.Tags = tagNames;
            model.DraftID = id;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveTagFromDraft(TagToDraftView model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            int tagId = dataServices.GetTagId(model.TagName);
            Draft draft = dataServices.GetDraft(model.DraftID);
            Article.RemoveTag(draft, tagId);
            dataServices.UpdateDraft(draft);
            return RedirectToAction("AllDrafts", "Drafts");
        }

        [HttpGet]
        public IActionResult CreateTemplateDraft()
        {
            DraftView model = new DraftView();
            model.TopicNames = dataServices.GetAllTopicAndSubTopicNames();
            return View(model);
        }

    }

}
