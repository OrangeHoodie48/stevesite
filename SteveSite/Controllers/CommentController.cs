﻿using Microsoft.AspNetCore.Mvc;
using SteveSite.Models;
using SteveSite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Controllers
{
    public class CommentController : Controller
    {
        IDataServices dataServices;

        public CommentController(IDataServices dataServices)
        {
            this.dataServices = dataServices;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddComment(CommentView view)
        {

            if (!ModelState.IsValid)
            {

                return RedirectToAction("ViewPost", "Posts", new { id = view.PostId });
            }
            Comment comment = new Comment();
            comment.Body = view.Body;
            comment.PostId = view.PostId;
            comment.UserName = view.UserName;
            comment.Date = DateTime.Now.ToString();
            dataServices.AddComment(comment);

            return RedirectToAction("ViewPost", "Posts", new { id = view.PostId });
        }

        public IActionResult RemoveComment(int id, int postId)
        {
            dataServices.RemoveComment(id);
            return RedirectToAction("ViewPost", "Posts", new { id = postId});
        }
    }
}
