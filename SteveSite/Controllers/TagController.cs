﻿using Microsoft.AspNetCore.Mvc;
using SteveSite.Models;
using SteveSite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SteveSite.Controllers
{
    public class TagController : Controller
    {
        IDataServices dataServices;
        public TagController(IDataServices dataServices)
        {
            this.dataServices = dataServices;
        }

        public IActionResult AllTags()
        {
            return View(dataServices.GetTags());
        }

        [HttpGet]
        public IActionResult CreateTag()
        {
            Tag tag = new Tag();
            return View(tag);
        }

        [HttpPost]
        [ValidateAntiForgeryTokenAttribute]
        public IActionResult CreateTag(Tag tag)
        {
            if (!ModelState.IsValid)
            {
                return View(tag);
            }

            dataServices.CreateTag(tag.TagName);
            return RedirectToAction("AllTags", "Tag");
        }


        public IActionResult RemoveTag(int id)
        {

            dataServices.RemoveTag(id);
            return RedirectToAction("AllTags", "Tag");
        }

        public IActionResult GetAllForTag(string tagName)
        {
            int tagId = dataServices.GetTagId(tagName);

            List<Post> posts = new List<Post>();
            foreach (Post post in dataServices.GetPosts())
            {
                if (String.IsNullOrEmpty(post.Tags)) continue;
                if (post.Tags.Contains(tagId + "Deljdf!@#")) posts.Add(post);
            }
            return View(posts);
        }
    }
}
